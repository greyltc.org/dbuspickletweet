from gi.repository import GLib
from gi.repository import Gio
from gi.repository import GObject
import xml.etree.ElementTree as ET
import pickle
import sys

class dbuspickletweet(GObject.Object):
  bus_name = "org.greyltc.dbuspickletweeter"
  signal_name = "tweet"
  ready = False

  def __init__(self, bus_name=bus_name, signal_name=signal_name):
    super().__init__()
    self.bus_name = bus_name
    self.signal_name = signal_name
    self.bus_name_path = '/' + self.bus_name.replace('.','/')

    # form the xml for the dbus server definition
    node = ET.Element("node")
    interface = ET.SubElement(node, "interface", name=self.bus_name)

    # here we define the actual signal we'll emit. ay is a byte array
    signal = ET.SubElement(interface, "signal", name=self.signal_name)
    ET.SubElement(signal, "arg", type="ay")

    # this method is just here for testing the service
    method = ET.SubElement(interface, "method", name="whoareyou")
    ET.SubElement(method, "arg", type="s", name="response", direction="out")
    ET.SubElement(method, "arg", type="s", name="whosasking", direction="in")

    xml = "<!DOCTYPE node PUBLIC '-//freedesktop//DTD D-BUS Object Introspection 1.0//EN' "
    xml += "'http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd'>"
    xml += ET.tostring(node, xml_declaration=False).decode()
    # for debugging:
    #tree = ET.ElementTree(node)
    #tree.write("filename.xml")

    # parse XML definition into dbus node info object
    self.server_info = Gio.DBusNodeInfo.new_for_xml(xml)

  @GObject.Signal
  def name_acquired(self):
      pass

  def handle_name_acquisition(self, connection, name):
    """called when the bus name acquisition is complete
    here we register the server's capabilities with the bus"""

    if name == self.bus_name:
      self.iface_connection = connection
      register_object_setup = {}
      register_object_setup['object_path'] = self.bus_name_path
      register_object_setup['interface_info'] = self.server_info.interfaces[0]
      register_object_setup['method_call_closure'] = self.handle_method_call
      register_object_setup['get_property_closure'] = None
      register_object_setup['set_property_closure'] = None
      self.iface_reg = connection.register_object(**register_object_setup)
      self.ready = True
      self.emit('name_acquired')

  def handle_method_call(self, connection, sender, object_path, interface_name, method_name, parameters, invocation):
    """handles all method calls called by clients over the bus"""
    if method_name == 'whoareyou':
      whosasking = parameters.unpack()[0]
      response = f"Hello {whosasking}, I'm {self.bus_name}!"
      invocation.return_value(GLib.Variant("(s)", [response]))
    else:
      print(f"DANGER: Unexpected method name: {method_name}")

  def dbus_emit(self, to_tweet=None):
    """emits a signal on the bus with a payload attached
    returns True when the emission was attempted (the service was ready)"""
    ret_code = False
    if self.ready:
      emit_signal_setup = {}
      emit_signal_setup['destination_bus_name'] = None
      emit_signal_setup['object_path'] = self.bus_name_path
      emit_signal_setup['interface_name'] = self.bus_name
      emit_signal_setup['signal_name'] = self.signal_name
      payload = pickle.dumps(to_tweet, pickle.HIGHEST_PROTOCOL)
      emit_signal_setup['parameters'] = GLib.Variant("(ay)", [payload])
      self.iface_connection.emit_signal(**emit_signal_setup)
      ret_code = True
    return ret_code
  
  def dbus_connect(self):
    bus_get_setup = {}
    bus_get_setup['bus_type'] = Gio.BusType.SESSION
    bus_get_setup['cancellable'] = None
    session_bus_connection = Gio.bus_get_sync(**bus_get_setup)

    # Start acquiring name on the bus
    bus_own_name_setup = {}
    bus_own_name_setup['connection'] = session_bus_connection
    bus_own_name_setup['name'] = self.bus_name
    bus_own_name_setup['flags'] = Gio.BusNameOwnerFlags.DO_NOT_QUEUE
    bus_own_name_setup['name_acquired_closure'] = self.handle_name_acquisition
    bus_own_name_setup['name_lost_closure'] = None
    self.bus_reg = Gio.bus_own_name_on_connection(**bus_own_name_setup)
  
  def dbus_disconnect(self):
    self.ready = False
    if hasattr(self, "iface_connection"):
      if hasattr(self.iface_connection, "unregister_object"):
        self.iface_connection.unregister_object(self.iface_reg)
    if hasattr(self, "bus_reg"):
      Gio.bus_unown_name(self.bus_reg)

# for testing the class
def test():
  global ret_val
  ret_val = -1
  bus_name = "org.greyltc.dbpt"
  signal_name = "beep"

  test_service_duration = 9  # seconds

  dbt = dbuspickletweet(bus_name=bus_name, signal_name=signal_name)

  # let's make some helper callback functions for testing
  # bus connection helper
  def connect_to_bus():
    print(f"Registering our service ({bus_name}) on the bus...")
    dbt.dbus_connect()
    dbt.connect("name_acquired", on_bus_name_acquired)

  # gets called when the service is properly registerd on the bus
  def on_bus_name_acquired(instance):
    print(f"Registration complete!")
    n = 2
    print(f"Emitting signal {signal_name} every {n} seconds...")
    GLib.timeout_add_seconds(n, tweeter)  # emit our signal tweet every this many seconds

  # function to do the message tweeting
  def tweeter():
    global ret_val
    if dbt.dbus_emit(to_tweet="BEEP! BEEP!"):
      print("Emission!")
      ret_val = 0
    else:
      print("No emission")
    return True  # true retrun means the timeout will wrap

  # bus disconnection helper
  def disconnect_from_bus():
    print(f"Deregistering our service ({bus_name})")
    dbt.dbus_disconnect()

  # loop exit helper
  def quit_a_loop(this_loop):
    print(f"Quitting main loop")
    this_loop.quit()

  # create a new main loop opject
  loop = GLib.MainLoop.new(None, False)

  # one second after the loop starts, we'll initiate the connection to the bus
  GLib.timeout_add_seconds(1, connect_to_bus)

  # schedule disconnection 
  GLib.timeout_add_seconds(test_service_duration-1, disconnect_from_bus)

  # schedule termination of the test loop
  GLib.timeout_add_seconds(test_service_duration, quit_a_loop, loop)  # call loop.quit when we're done

  # run the main loop
  loop.run()

  if ret_val == 0:
    print(f"Test passed")
  else:
    print(f"Test failed: {ret_val}")

  print(f"Testing complete.")
  return ret_val

if __name__ == "__main__":
    exit_code = test()
    sys.exit(exit_code)
